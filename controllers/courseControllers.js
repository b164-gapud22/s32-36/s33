const Course = require("../models/Course");


// Create a new course
/*
Steps:
1. Create a new Course object
2. Save to the database
3. Error handling

*/



// ACTIVITY
// module.exports.addCourse = (reqBody) => {
// 	console.log(reqBody);

	
// 		// Create a new Object
// 		let newCourse = new Course({
// 			name: reqBody.name,
// 			description: reqBody.description,
// 			price: reqBody.price
// 		})

// 		return newCourse.save().then((course, error) => {
// 			if(error){
// 				return false;
// 			}else{
// 				return true;
// 			}
// 		})
// }

// ANSWER
module.exports.addCourse = (reqBody) => {
	console.log(reqBody);

	
		// Create a new Object
		let newCourse = new Course({
			name: reqBody.course.name,
			description: reqBody.course.description,
			price: reqBody.course.price
		})

		return newCourse.save().then((course, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
}



// Retrieving ALL courses
module.exports.getAllCourses = () => {
	return Course.find({}).then( result => {
		return result;
	})
}


// Retrieve all ACTIVE courses

module.exports.getAllActive = () => {
	return Course.find({ isActive: true }).then(result => {
		return result;
	})
}


// Retrieve SPECIFIC course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result;
	})
}



// Update a course
/*
Steps:
1. Create variable which will contain the information retrieved from the  request body
2. Find and update course using the course ID
*/

module.exports.updateCourse = (courseId, reqBody) => {
	// specify the properties of the doc to be updated
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	// findByIdAndUpdate
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
		// course not updated
		if(error){
			return false;
		} else {
			// course updated successfully
			return true;
		}
	})
}



// ACTIVITY

module.exports.archiveCourse = (reqBody) => {
	return Course.findById(reqBody).then(result => {
		if(result == null){
			return false;
		}else{
			result.isActive = false;
			return result;
		}
	})
}











