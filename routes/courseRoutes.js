const express = require("express");
const router = express.Router();
const auth = require("../auth")

const CourseController = require("../controllers/courseControllers")


// ACTIVITY
// router.post("/", auth.verify, (req, res) => {

// 	const userData = auth.decode(req.headers.authorization);
// 	if (userData.isAdmin != true){
// 		return false;
// 	}else{
// 		CourseController.addCourse(req.body).then(result => res.send(result))
// 	}

// })


// ANSWER
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	if(data.isAdmin){
		CourseController.addCourse(data).then(result => res.send(result));
	}else{
		res.send({ auth: "You're not an admin"})
	};
});


// Retrieving ALL courses
router.get("/all", (req, res) => {
	CourseController.getAllCourses().then(result => res.send(result));
})




// Retrieving all ACTIVE course
router.get("/", (req, res) => {
	CourseController.getAllActive().then(result => res.send(result))
});


// Retrieving a SPECIFIC Course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);

	CourseController.getCourse(req.params.courseId).then(result => res.send(result))
});



// Update a course
router.put("/:courseId", auth.verify,(req, res) => {


	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		CourseController.updateCourse(req.params.courseId, req.body).then(result => res.send(result));
	}else{
		res.send(false)
	}
});



// ACTIVITY

/*

1. Determine if the course exist in the database.
2. Create authentication.
3. Change the course to inactive.


*/

router.put("/:courseId/archive", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		CourseController.archiveCourse(req.params.courseId).then(result => res.send(result));
	}else{
		res.send(`You're not an admin`)
	};


})



















module.exports = router;